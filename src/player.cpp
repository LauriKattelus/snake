#include "player.h"
#include "init.h"

Piece::Piece(){
}

Piece::Piece(int x, int y){
    piece.x = x;
    piece.y = y;
    piece.w = TILE;
    piece.h = TILE;
}

Player::Player(){
    Timer timer;
    timer.reset_timer();
    move_delay = 50;
    direction = right;

    alive = true;
    int length = 10;

    for(int i = 0; i < length; i++){
        Piece piece(SCREEN_WIDTH/2,SCREEN_HEIGHT/2);
        snake.push_back(piece);
    }

}

void Player::grow(){
    Piece piece(snake[0].piece.x, snake[0].piece.y);
    snake.push_back(piece);
}

void Player::draw(){
    for(int i = 0; i < snake.size(); i++){
        SDL_SetRenderDrawColor(renderer, 0xFF,0x00,0x00,0x00);
        SDL_RenderFillRect(renderer, &snake[i].piece);
    }
}

void Player::move(){
    if((SDL_GetTicks() - timer.start_time) > move_delay){
        direction = new_direction;
        switch(direction){
        case up:
            if(snake[0].piece.y > 0){
                snake[0].piece.y -= TILE;
            } else {
                alive = false;
            }
            break;

            case down:
            if(snake[0].piece.y < SCREEN_HEIGHT - TILE){
                snake[0].piece.y += TILE;
            } else {
                alive = false;
            } 
            break;

            case left:
            if(snake[0].piece.x > 0){
                snake[0].piece.x -= TILE;
            } else {
                alive = false;
            }
            break;

            case right:
            if(snake[0].piece.x < SCREEN_WIDTH - TILE){
                snake[0].piece.x += TILE;
            } else {
                alive = false;
            }
            break;
        }

        body_collision();
        move_body();

    timer.reset_timer();
    }
}

void Player::move_body(){
    int piece_x = snake[0].piece.x;
    int piece_y = snake[0].piece.y;

    for(int i = 1; i < snake.size(); i++){
        int swap_x = snake[i].piece.x;
        int swap_y = snake[i].piece.y;
        snake[i].piece.x = piece_x;
        snake[i].piece.y = piece_y;
        piece_x = swap_x;
        piece_y = swap_y;
    }
}

void Player::body_collision(){
    for(int i = 1; i < snake.size(); i++){
        if(get_x() == snake[i].piece.x && get_y() == snake[i].piece.y){
            alive = false;
        }
    }
}

int Player::get_x(){
    return snake[0].piece.x;
}

int Player::get_y(){
    return snake[0].piece.y;
}