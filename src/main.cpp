#include <iostream>
#include "init.h"
#include "input.h"
#include "player.h"
#include "food.h"

#define LOG(x) std::cout << x << std::endl

int main(){
    init();

    Player* player = new Player;
    Food* food = new Food;

    while(running){
        check_input(player,food);

        clear_screen();

        player->move();
        food->draw();
        player->draw();

        if(player->get_x() == food->get_x() && player->get_y() == food->get_y()){
            food->alive = false;
        }

        if(!player->alive){
            delete player;
            player = new Player;
        }

        if(!food->alive){
            delete food;
            food = new Food;
            for(int i = 0; i < player->snake.size(); i++){
                if(player->snake[i].piece.x == food->get_x() && player->snake[i].piece.y == food->get_y()){
                    delete food;
                    food = new Food;
                    i = 0;
                }
            }
            player->grow();
        }

        render_present();
    }

    close();

    return 0;
}