#ifndef INIT_H
#define INIT_H

#include <SDL2/SDL.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int TILE = 20;

enum Direction {up, down, left, right};

extern Direction new_direction;

extern SDL_Window* window;
extern SDL_Renderer* renderer;

extern bool running;

bool init();

bool close();

void clear_screen();

void render_present();

#endif