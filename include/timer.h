#ifndef TIMER_H
#define TIMER_H

#include <SDL2/SDL.h>

class Timer {
    public:
        Uint32 start_time;

        Timer();

        void reset_timer();
};

#endif