#ifndef PLAYER_H
#define PLAYER_H

#include <SDL2/SDL.h>
#include <vector>
#include "timer.h"
#include "init.h"

class Piece {
public:
    SDL_Rect piece;
    
    Piece();
    Piece(int x, int y);
};

class Player {
private:
    Timer timer;
    int move_delay;
    void move_body();
    void body_collision();

public:
    std::vector<Piece> snake;
    bool alive;

    Direction direction;

    Player();

    void grow();
    void draw();
    void move();
    int get_x();
    int get_y();
};

#endif