#ifndef INPUT_H
#define INPUT_H

#include <SDL2/SDL.h>
#include "init.h"
#include "player.h"
#include "food.h"

void check_input(Player* player = NULL, Food* food = NULL);

#endif